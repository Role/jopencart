/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_return_action表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|return_action_id    |INT(10)             |false |true    |NULL    |
|language_id         |INT(10)             |false |true    |0|
|name                |VARCHAR(64)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class ReturnAction extends Model<ReturnAction>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2468941483282246894L;
	/**
	 * 用于查询操作
	 */
	public static final ReturnAction ME = new ReturnAction();
	
}