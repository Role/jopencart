/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_language表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|language_id         |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(32)         |false |false   |NULL    |
|code                |VARCHAR(5)          |false |false   |NULL    |
|locale              |VARCHAR(255)        |false |false   |NULL    |
|image               |VARCHAR(64)         |false |false   |NULL    |
|directory           |VARCHAR(32)         |false |false   |NULL    |
|sort_order          |INT(10)             |false |false   |0|
|status              |BIT(0)              |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Language extends Model<Language>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2409181483282240918L;
	/**
	 * 用于查询操作
	 */
	public static final Language ME = new Language();
	
}