/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_layout_route表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|layout_route_id     |INT(10)             |false |true    |NULL    |
|layout_id           |INT(10)             |false |false   |NULL    |
|store_id            |INT(10)             |false |false   |NULL    |
|route               |VARCHAR(255)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class LayoutRoute extends Model<LayoutRoute>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2413051483282241305L;
	/**
	 * 用于查询操作
	 */
	public static final LayoutRoute ME = new LayoutRoute();
	
}