/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_country表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|country_id          |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(128)        |false |false   |NULL    |
|iso_code_2          |VARCHAR(2)          |false |false   |NULL    |
|iso_code_3          |VARCHAR(3)          |false |false   |NULL    |
|address_format      |TEXT(65535)         |false |false   |NULL    |
|postcode_required   |BIT(0)              |false |false   |NULL    |
|status              |BIT(0)              |false |false   |1|
+--------------------+--------------------+------+--------+--------+--------

 */
public class Country extends Model<Country>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2363941483282236394L;
	/**
	 * 用于查询操作
	 */
	public static final Country ME = new Country();
	
}