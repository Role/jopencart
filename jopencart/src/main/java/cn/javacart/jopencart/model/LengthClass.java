/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_length_class表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|length_class_id     |INT(10)             |false |true    |NULL    |
|value               |DECIMAL(15)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class LengthClass extends Model<LengthClass>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2414411483282241441L;
	/**
	 * 用于查询操作
	 */
	public static final LengthClass ME = new LengthClass();
	
}