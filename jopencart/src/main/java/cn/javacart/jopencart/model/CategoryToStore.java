/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_category_to_store表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|category_id         |INT(10)             |false |true    |NULL    |
|store_id            |INT(10)             |false |true    |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class CategoryToStore extends Model<CategoryToStore>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2362521483282236252L;
	/**
	 * 用于查询操作
	 */
	public static final CategoryToStore ME = new CategoryToStore();
	
}