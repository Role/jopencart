package cn.javacart.jopencart.controller.catalog.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfplugin.kit.mockrender.MockRenderKit;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;
import cn.javacart.jopencart.library.SessionConfigService;
import cn.javacart.jopencart.model.Category;

/**
 * 页头
 * @author farmer
 *
 */
public class CommonHeaderModule extends JOpencartModule{

	public CommonHeaderModule(JOpencartController controller) {
		super(controller);
	}

	@Override
	public String render() {
		/*文档相关参数*/
		dataMap.put("title", MemoryUtils.valueOf(document.getTitle()));
		dataMap.put("description", MemoryUtils.valueOf(document.getDescription()));
		dataMap.put("keywords", MemoryUtils.valueOf(document.getKeywords()));
		dataMap.put("links", MemoryUtils.valueOf(document.getLinks()));
		dataMap.put("styles", MemoryUtils.valueOf(document.getStyles()));
		dataMap.put("scripts", MemoryUtils.valueOf(document.getScripts(null)));
		dataMap.put("analytics", MemoryUtils.valueOf( new HashMap<String, String>()));
		dataMap.put("lang", language.get("code"));
		dataMap.put("direction", language.get("direction"));
		dataMap.put("logo", MemoryUtils.valueOf(((String)config.get("context_path")).concat("image/").concat((String)config.get("config_logo"))));
		language.load("/catalog/language/{0}/common/header.php");
		dataMap.put("text_shopping_cart", language.get("text_shopping_cart"));
		dataMap.put("text_account", language.get("text_account"));
		dataMap.put("text_register", language.get("text_register"));
		dataMap.put("text_login", language.get("text_login"));
		dataMap.put("text_order", language.get("text_order"));
		dataMap.put("text_transaction", language.get("text_transaction"));
		dataMap.put("text_download", language.get("text_download"));
		dataMap.put("text_logout", language.get("text_logout"));
		dataMap.put("text_checkout", language.get("text_checkout"));
		dataMap.put("text_category", language.get("text_category"));
		dataMap.put("text_all", language.get("text_all"));
		/*
		 * $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		 * */
		dataMap.put("text_wishlist",MemoryUtils.valueOf(String.format(language.getStr("text_wishlist"), 0)));
		
		
		dataMap.put("home",url.link("common/home", ""));
		dataMap.put("wishlist", url.link("account/wishlist", ""));
		dataMap.put("logged", MemoryUtils.valueOf(customer.isLogged()));	//登录状态
		dataMap.put("account", url.link("account/account", ""));
		dataMap.put("register", url.link("account/register", ""));
		dataMap.put("login", url.link("account/login", ""));
		dataMap.put("order", url.link("account/order", ""));
		dataMap.put("transaction", url.link("account/transaction", ""));
		dataMap.put("download", url.link("account/download", ""));
		dataMap.put("logout", url.link("account/logout", ""));
		dataMap.put("shopping_cart", url.link("checkout/cart", ""));
		dataMap.put("checkout", url.link("checkout/checkout", ""));
		dataMap.put("contact", url.link("information/contact", ""));
		dataMap.put("telephone", MemoryUtils.valueOf(config.get("config_telephone")));
		
		List<Map<String, Object>> categorieList = new ArrayList<Map<String,Object>>();
		List<Category> categories = Category.ME.getCategories(0);
		for (Category category : categories) {
			if(category.get("top") != null){
				List<Map<String, Object>> childrenData = new ArrayList<Map<String,Object>>();
				List<Category> children = Category.ME.getCategories(category.getInt("category_id"));
				for (Category child : children) {
					Map<String, Object> childrenDataItem = new HashMap<String, Object>();
					childrenDataItem.put("name", child.get("name")+" ("+Db.queryLong("SELECT\n" + 
							"	COUNT( DISTINCT p.product_id ) AS total\n" + 
							"FROM\n" + 
							"	joc_category_path cp\n" + 
							"LEFT JOIN joc_product_to_category p2c ON\n" + 
							"	(\n" + 
							"		cp.category_id = p2c.category_id\n" + 
							"	)\n" + 
							"LEFT JOIN joc_product p ON\n" + 
							"	(\n" + 
							"		p2c.product_id = p.product_id\n" + 
							"	)\n" + 
							"LEFT JOIN joc_product_description pd ON\n" + 
							"	(\n" + 
							"		p.product_id = pd.product_id\n" + 
							"	)\n" + 
							"LEFT JOIN joc_product_to_store p2s ON\n" + 
							"	(\n" + 
							"		p.product_id = p2s.product_id\n" + 
							"	)\n" + 
							"WHERE\n" + 
							"	pd.language_id = ?\n" + 
							"	AND p.status = '1'\n" + 
							"	AND p.date_available <= NOW()\n" + 
							"	AND p2s.store_id = '0'\n" + 
							"	AND cp.path_id = ?",SessionConfigService.get("config_language_id") , child.get("category_id"))+")");
					childrenDataItem.put("href", url.link("product/category", "path="+category.getInt("category_id")+"_"+child.getInt("category_id")));					
					childrenData.add(childrenDataItem);
				}
				Map<String,Object> categoryMap = new HashMap<String, Object>();
				categoryMap.put("name", category.get("name"));
				categoryMap.put("children", childrenData);
				categoryMap.put("column", (category.get("column") != null && category.getInt("column") != 0 )? category.get("column") : 1);
				categoryMap.put("href", url.link("product/category", "path="+category.getInt("category_id")));
				categorieList.add(categoryMap);
			}
		}
		dataMap.put("categories", MemoryUtils.valueOf(categorieList));
		dataMap.put("class",MemoryUtils.valueOf("common-home"));
		/*页头组件*/
		dataMap.put("language", MemoryUtils.valueOf(load.module(new CommonLanguageModule(controller))));	//加载语言
		dataMap.put("currency", MemoryUtils.valueOf(load.module(new CommonCurrencyModule(controller))));	//加载币种
		dataMap.put("search", MemoryUtils.valueOf(load.module(new CommonSearchModule(controller))));	//加载搜索框
		dataMap.put("cart", MemoryUtils.valueOf(load.module(new CommonCartModule(controller))));	//加载购物车		
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/common/header.tpl", dataMap));
	}

}
