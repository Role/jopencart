package cn.javacart.jopencart.controller.catalog.account;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.controller.catalog.common.CommonColumnLeftModule;
import cn.javacart.jopencart.controller.catalog.common.CommonColumnRightModule;
import cn.javacart.jopencart.controller.catalog.common.CommonContentBottomModule;
import cn.javacart.jopencart.controller.catalog.common.CommonContentTopModule;
import cn.javacart.jopencart.controller.catalog.common.CommonFooterModule;
import cn.javacart.jopencart.controller.catalog.common.CommonHeaderModule;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.util.ChainMap;

/**
 * 用户中心
 * @author farmer
 *
 */
public class AccountAccountController extends JOpencartController{

	/**
	 * 用户中心首页
	 */
	public void index(){
		if(!customer.isLogged()){
			setSessionAttr("redirect", url.linkStr("account/account", null));
			redirect(url.linkStr("account/login", null));
			return;
		}
		language.load("/catalog/language/{0}/account/account.php");
		document.setTitle(language.getStr("heading_title"));
		List<Map<String, Object>> breadcrumbs = new ArrayList<Map<String,Object>>();
		breadcrumbs.add(ChainMap.createMap().put("text", language.getStr("text_home")).put("href", url.linkStr("common/home", null)).toMap());
		breadcrumbs.add(ChainMap.createMap().put("text", language.getStr("text_account")).put("href", url.linkStr("account/account", null)).toMap());
		dataMap.put("breadcrumbs", MemoryUtils.valueOf(breadcrumbs));
		String success = (String) (getSessionAttr("success") == null ? "" : getSessionAttr("success"));
		dataMap.put("success", MemoryUtils.valueOf(success));
		dataMap.put("heading_title",language.get("heading_title"));
		dataMap.put("text_my_account",language.get("text_my_account"));
		dataMap.put("text_my_orders",language.get("text_my_orders"));
		dataMap.put("text_my_newsletter",language.get("text_my_newsletter"));
		dataMap.put("text_edit",language.get("text_edit"));
		dataMap.put("text_password",language.get("text_password"));
		dataMap.put("text_address",language.get("text_address"));
		dataMap.put("text_credit_card",language.get("text_credit_card"));
		dataMap.put("text_wishlist",language.get("text_wishlist"));
		dataMap.put("text_order",language.get("text_order"));
		dataMap.put("text_download",language.get("text_download"));
		dataMap.put("text_reward",language.get("text_reward"));
		dataMap.put("text_return",language.get("text_return"));
		dataMap.put("text_transaction",language.get("text_transaction"));
		dataMap.put("text_newsletter",language.get("text_newsletter"));
		dataMap.put("text_recurring",language.get("text_recurring"));
		
		dataMap.put("edit" , url.link("account/edit",null));
		dataMap.put("password" , url.link("account/password",null));
		dataMap.put("address" , url.link("account/address",null));
		
		dataMap.put("wishlist" , url.link("account/wishlist",null));
		dataMap.put("order" , url.link("account/order",null));
		dataMap.put("download" , url.link("account/download",null));
		
		if ("1".equals(config.get("reward_status"))) {
			dataMap.put("reward" , url.link("account/reward",null));
		} else {
			dataMap.put("reward" , MemoryUtils.valueOf(""));
		}
		dataMap.put("return" , url.link("account/return",null));
		dataMap.put("transaction" , url.link("account/transaction",null));
		dataMap.put("newsletter" , url.link("account/newsletter",null));
		dataMap.put("recurring" , url.link("account/recurring",null));
		
		dataMap.put("column_left", MemoryUtils.valueOf(load.module(new CommonColumnLeftModule(this))));
		dataMap.put("column_right", MemoryUtils.valueOf(load.module(new CommonColumnRightModule(this))));
		dataMap.put("content_top", MemoryUtils.valueOf(load.module(new CommonContentTopModule(this))));
		dataMap.put("content_bottom", MemoryUtils.valueOf(load.module(new CommonContentBottomModule(this))));
		dataMap.put("footer", MemoryUtils.valueOf(load.module(new CommonFooterModule(this))));
		dataMap.put("header", MemoryUtils.valueOf(load.module(new CommonHeaderModule(this))));
		render(new PhpRender("/catalog/view/theme/default/template/account/account.tpl", dataMap));
	}
	
}
