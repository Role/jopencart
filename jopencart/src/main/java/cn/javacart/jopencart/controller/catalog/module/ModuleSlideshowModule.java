package cn.javacart.jopencart.controller.catalog.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;
import cn.javacart.jopencart.library.SessionConfigService;
import cn.javacart.jopencart.model.BannerImage;

import com.alibaba.fastjson.JSON;
import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 幻灯片
 * @author farmer
 *
 */
public class ModuleSlideshowModule extends JOpencartModule{

	private String extra = null;
	
	public ModuleSlideshowModule(JOpencartController controller) {
		super(controller);
	}
	
	public ModuleSlideshowModule(JOpencartController controller,Object extra) {
		super(controller);
		this.extra = (String) extra;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String render() {
		document.addStyle("catalog/view/javascript/jquery/owl-carousel/owl.carousel.css", null,null);
		document.addScript("catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js", null);
		HashMap<String,String> settingMap = JSON.parseObject(extra, HashMap.class);
		String bannerId = settingMap.get("banner_id");
		List<Map<String, Object>> bannerList = new ArrayList<Map<String,Object>>();
		List<BannerImage> bannerImages = BannerImage.ME.find("SELECT * FROM joc_banner_image bi LEFT JOIN joc_banner_image_description bid ON (bi.banner_image_id  = bid.banner_image_id) WHERE bi.banner_id = ? AND bid.language_id = ? ORDER BY bi.sort_order ASC",bannerId,SessionConfigService.get("config_language_id"));
		for (BannerImage bannerImage : bannerImages) {
			Map<String, Object> bannerItem = new HashMap<String, Object>();
			bannerItem.put("title", bannerImage.get("title"));
			bannerItem.put("link", bannerImage.get("link"));
			String uri = bannerImage.getStr("image");
			bannerItem.put("image", uri.substring(0, uri.lastIndexOf("."))+"_"+settingMap.get("width")+"x"+settingMap.get("height")+uri.substring(uri.lastIndexOf("."),uri.length()));
			bannerList.add(bannerItem);
		}
		dataMap.put("banners", MemoryUtils.valueOf(bannerList));
		dataMap.put("module", MemoryUtils.valueOf(module.get()+1));
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/module/slideshow.tpl", dataMap));
	}

}
