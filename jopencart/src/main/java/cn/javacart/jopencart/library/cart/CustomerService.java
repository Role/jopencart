package cn.javacart.jopencart.library.cart;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;

import cn.javacart.jopencart.library.SessionService;
import cn.javacart.jopencart.model.CustomerIp;
import cn.javacart.jopencart.tools.WebKit;

/**
 * 顾客基本信息Session
 * @author farmer
 *
 */
public class CustomerService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2900326595144430407L;
	/**
	 * 客户ID
	 */
	private Integer customerId;
	/**
	 * 姓
	 */
	private String firstname;
	/**
	 * 名
	 */
	private String lastname;
	/**
	 * 用户组ID
	 */
	private Integer customerGroupId;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 电话
	 */
	private String telephone;
	/**
	 * 传真
	 */
	private String fax;
	/**
	 * 
	 */
	private boolean newsletter;
	/**
	 * 默认地址ID
	 */
	private Integer addressId;
	
	
	static ThreadLocal<CustomerService> threadLocal = new ThreadLocal<CustomerService>();
	
	/**
	 * 初始化时加入线程变量
	 */
	public CustomerService(){
		threadLocal.set(this);
	}
	
	/**
	 * 获取当前对象
	 * @return
	 */
	public static CustomerService me(){
		return threadLocal.get();
	}
	
	/**
	 * 登录
	 * @param email
	 * @param password
	 * @return
	 */
	public boolean login(String email, String password){
		return login(email, password, false);
	}
	/**
	 * 登录
	 * @param email
	 * @param password
	 * @param override
	 * @return
	 */
	public boolean login(String email, String password, boolean override){
		cn.javacart.jopencart.model.Customer customerInfo = 
				cn.javacart.jopencart.model.Customer.ME.findFirst("SELECT * FROM joc_customer WHERE LOWER(email) = ? AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1(?))))) OR password = ?) AND status = '1' AND approved = '1'",
				email,password,DigestUtils.md5Hex(password));
		if(customerInfo != null){
			Integer custId = customerInfo.getInt("customer_id");
			SessionService.get().setAttribute("customer_id", custId);
			this.customerId = custId;
			this.firstname = customerInfo.getStr("firstname");
			this.lastname = customerInfo.getStr("lastname");
			this.customerGroupId = customerInfo.getInt("customer_group_id");
			this.email = customerInfo.getStr("email");
			this.telephone = customerInfo.getStr("telephone");
			this.fax = customerInfo.getStr("fax");
			this.newsletter = customerInfo.getBoolean("newsletter");
			this.addressId = customerInfo.getInt("address_id");
			String remoteAddr = WebKit.getIp();
			customerInfo.set("ip", remoteAddr).update(); //更新登录IP
			//存储登录IP记录
			CustomerIp customerIp = CustomerIp.ME.findFirst("SELECT * FROM `joc_customer_ip` WHERE customer_id = ? and ip = ?", custId , remoteAddr);
			if(customerIp == null){
				new CustomerIp().set("customer_id", custId).set("ip", remoteAddr)
								.set("date_added", new Date()).save();
			}
			return true;
		}
		return false;
	}
	
	/**
	 * 是否登录状态
	 * @return
	 */
	public boolean isLogged(){
		return (customerId!=null && customerId!=0);
	}
	
	/**
	 * 注销
	 */
	public void logout() {
		SessionService.get().setAttribute("customer_id", null);
		SessionService.get().removeAttribute("customer_id");
		this.customerId = null;
		this.firstname = null;
		this.lastname = null;
		this.customerGroupId = null;
		this.email = null;
		this.telephone = null;
		this.fax = null;
		this.newsletter = false;
		this.addressId = null;
	}
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Integer getCustomerGroupId() {
		return customerGroupId;
	}
	public void setCustomerGroupId(Integer customerGroupId) {
		this.customerGroupId = customerGroupId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public boolean isNewsletter() {
		return newsletter;
	}
	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
}
