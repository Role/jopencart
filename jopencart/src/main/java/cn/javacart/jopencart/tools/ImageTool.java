package cn.javacart.jopencart.tools;

import com.jfinal.kit.StrKit;

public class ImageTool {

	/**
	 * 调整图片大小
	 * @param uri
	 * @param w
	 * @param h
	 * @return
	 */
	public static String resize(String uri ,Object w,Object h){
		if(StrKit.isBlank(uri)){
			return null;
		}
		return uri.substring(0, uri.lastIndexOf("."))+"_"+w+"x"+h+uri.substring(uri.lastIndexOf("."),uri.length());
	}
	
}
