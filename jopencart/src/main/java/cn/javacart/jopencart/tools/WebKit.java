package cn.javacart.jopencart.tools;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.kit.StrKit;

import cn.javacart.jopencart.library.RequestService;

public class WebKit {
	
	/***
	 * 获取IP
	 * @return
	 */
	public static String getIp(){
		HttpServletRequest request = RequestService.get();
		String ip = request.getRemoteAddr();
		if(Arrays.asList("127.0.0.1","localhost").contains(ip)){
			ip = request.getHeader("x-forwarded-for");
			if(StrKit.isBlank(ip) || "unknown".equalsIgnoreCase(ip)){
				ip = request.getHeader("Proxy-Client-IP");
			}
			if(StrKit.isBlank(ip) || "unknown".equalsIgnoreCase(ip)){
				ip = request.getHeader("WL-Proxy-Client-IP");
			}
			if(StrKit.isBlank(ip) || "unknown".equalsIgnoreCase(ip)){
				ip = request.getRemoteAddr();
			}
		}
		return ip;
	}

}
