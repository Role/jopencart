package cn.javacart.jopencart.tools;

import com.jfinal.kit.StrKit;

public class SetKit {

	/**
	 * 是否设置
	 * @param v
	 * @return
	 */
	public static boolean isset(String v){
		return StrKit.notBlank(v) && !"0".equals(v);
	}
	
	/**
	 * 是否设置
	 * @param v
	 * @return
	 */
	public static boolean isset(Integer v){
		return v != null && v != 0;
	}

	/**
	 * 是否定义
	 * @param v
	 * @return
	 */
	public static boolean isset(Object v) {
		return v != null && StrKit.notBlank(v+"") && !"0".equals(v);
	}
}
